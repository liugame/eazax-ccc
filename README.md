# Eazax-CCC



## 介绍

Cocos Creator 游戏开发脚手架，持续维护中。



## 目录

- component 常用组件
  - BackgroundFitter.ts 背景适配组件
  - HollowOut.ts 镂空效果组件
  - RotateAround.ts 围绕旋转组件
  - ScreenAdapter.ts 屏幕适配组件
  - TouchBlocker.ts 点击屏蔽组件


- constant 内置常量
  - Events.ts 事件常量


- core 核心脚本
  - AudioPlayer.ts 音频播放类
  - GameEvent.ts 事件监听发送类
  - Navigator.ts 场景管理类


- declaration 声明文件
  - cc.d.ts 扩展 cc 命名空间
  - editor.d.ts 编辑器命名空间
  - extension.d.ts 基础类型扩展声明
  - wx.d.ts 微信命名空间


- extension 扩展实现
  - extension.d.ts 基础类型扩展实现


- localization 本地化组件
  - LocalizationBase.ts 多语言组件基类
  - LocalizationLabelString.ts 多语言文本组件
  - LocalizationSpriteFrame.ts 多语言精灵组件


- misc 杂项
  - EditorAsset.ts 编辑器资源类


- resources 资源文件
  - effects Shader 文件
    - eazax-hollowout.effect 镂空 Shader
    - eazax-hollowout-circle.effect 镂空（圆形） Shader
    - eazax-hollowout-rect.effect 镂空（矩形） Shader
    - eazax-silhouette.effect 剪影 Shader


- util 工具
  - CalUtil.ts 计算工具


## 环境

引擎：Cocos Creator 2.3.3

语言：TypeScript



## 关于

作者：陈皮皮（ifaswind）

公众号：菜鸟小栈

![qrcode](https://gitee.com/ifaswind/image-storage/raw/master/weixin/qrcode.png)
