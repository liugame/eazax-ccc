interface String {

    /**
     * 翻译
     */
    translate(): string;

}

interface Array<T> {

    /**
     * 数组中是否包含目标
     * @param item item
     */
    include(item: T): boolean;

}
